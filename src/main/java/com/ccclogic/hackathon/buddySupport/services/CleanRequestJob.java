package com.ccclogic.hackathon.buddySupport.services;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.ccclogic.hackathon.buddySupport.dao.CustomerRepository;
import com.ccclogic.hackathon.buddySupport.models.Customer;
import com.ccclogic.hackathon.buddySupport.models.SupportStatus;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class CleanRequestJob {

	@Scheduled(fixedDelay = 5000)
	public void clean() {
		List<Customer> wasted = customers.findByStatus(SupportStatus.AWAITING_RESPONSE);
		log.debug(wasted.size() + " are resetted now");
		DateTime now = new DateTime().minusMinutes(5);
		for (Customer nothelped : wasted) {
			if(nothelped.getBusyWith().getConnectedAt().isBefore(now))
			nothelped.setBusyWith(null);
			nothelped.setStatus(SupportStatus.NOT_CONTACTED);
			template.save(nothelped);
		}
	}

	@Autowired
	private CustomerRepository customers;
	
	@Autowired
	private MongoTemplate template;
}
