package com.ccclogic.hackathon.buddySupport.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import com.ccclogic.hackathon.buddySupport.dao.ConnectionRequestRepository;
import com.ccclogic.hackathon.buddySupport.dao.CustomerRepository;
import com.ccclogic.hackathon.buddySupport.models.ConnectRequest;
import com.ccclogic.hackathon.buddySupport.models.Customer;
import com.ccclogic.hackathon.buddySupport.models.SupportStatus;
import com.skype.ChatMessage;
import com.skype.ChatMessageAdapter;
import com.skype.Skype;
import com.skype.SkypeException;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ChatListner extends ChatMessageAdapter {

	private Map<String, String> connections;

	public ChatListner() {
		connections = new HashMap<>();
	}

	public void chatMessageReceived(ChatMessage received) throws SkypeException {

		String responseTo = connections.get(received.getSenderId());
		if (responseTo == null) {
			if (connections.values().contains(received.getSenderId())) {
				if (exitCondition(received)) {
					for (Entry<String, String> takinghelp : connections.entrySet()) {
						if (takinghelp.getValue().equalsIgnoreCase(received.getSenderId())) {
							disconnect(takinghelp.getKey());
						}
					}
				}
				return;
			}
			ConnectRequest respondedTo = requests.findOne(received.getContent());
			if(respondedTo==null)
				return;
			Customer goodguy = customers.findOne(received.getSenderId());
			respondedTo.setServedBy(goodguy.getSkypeId());
			goodguy.setStatus(SupportStatus.CONNECTED);

			log.debug("Pairing " + goodguy.getSkypeId() + " for " + respondedTo.getId());
			// send them into a websocket chat room
			String[] skypeIds = { respondedTo.getFromSkypeId(), received.getSenderId() };
			Skype.chat(skypeIds).send(respondedTo.getQuestion());
			template.save(goodguy);
			template.save(respondedTo);
			connections.put(received.getSenderId(), respondedTo.getFromSkypeId());
			// change status for rest
			List<Customer> wasted = customers.findByStatusAndBusyWith(SupportStatus.AWAITING_RESPONSE,
					respondedTo.getId());
			for (Customer resetUser : wasted) {
				resetUser.setBusyWith(null);
				resetUser.setStatus(SupportStatus.NOT_CONTACTED);
				template.save(resetUser);
			}
		} else if (exitCondition(received)) {
			disconnect(received.getSenderId());

		} else if (received.getContent().equalsIgnoreCase("#wtf#")) {
			connections.put(received.getSenderId(), null);
			Customer goodguy = customers.findOne(received.getSenderId());
			goodguy.setStatus(SupportStatus.NEGATIVE_RESPONSE);

			log.debug("UNPairing " + goodguy.getSkypeId());
			// send them into a websocket chat room
			template.save(goodguy);

		}
	}

	private boolean exitCondition(ChatMessage received) throws SkypeException {

		return received.getContent().equalsIgnoreCase("#bye#") || received.getContent().equalsIgnoreCase("#quit#")
				|| received.getContent().equalsIgnoreCase("#exit#");
	}

	private void disconnect(String goodguyid) {
		connections.put(goodguyid, null);
		Customer goodguy = customers.findOne(goodguyid);
		goodguy.setStatus(SupportStatus.NOT_CONTACTED);

		log.debug("UNPairing " + goodguy.getSkypeId());
		// send them into a websocket chat room
		template.save(goodguy);
	}

	@Autowired
	private ConnectionRequestRepository requests;

	@Autowired
	private CustomerRepository customers;

	@Autowired
	private MongoTemplate template;

}
