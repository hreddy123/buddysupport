package com.ccclogic.hackathon.buddySupport.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.ccclogic.hackathon.buddySupport.models.CustomerConnection;


public interface CustomerConnectionRepository extends MongoRepository<CustomerConnection, String> {



}
