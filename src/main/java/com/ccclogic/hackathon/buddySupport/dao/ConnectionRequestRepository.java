package com.ccclogic.hackathon.buddySupport.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.ccclogic.hackathon.buddySupport.models.ConnectRequest;


public interface ConnectionRequestRepository extends MongoRepository<ConnectRequest, String> {



}
