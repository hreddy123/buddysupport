package com.ccclogic.hackathon.buddySupport.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.ccclogic.hackathon.buddySupport.models.Customer;
import com.ccclogic.hackathon.buddySupport.models.SupportStatus;

public interface CustomerRepository extends PagingAndSortingRepository<Customer, String> {

public List<Customer> findByStatusAndSkypeIdIn(SupportStatus status,List<String> ids);
public List<Customer> findByStatus(SupportStatus status);
public List<Customer> findByStatusAndBusyWith(SupportStatus status,String reqId);

}
