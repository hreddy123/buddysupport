package com.ccclogic.hackathon.buddySupport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.ccclogic.hackathon.buddySupport.services.ChatListner;
import com.skype.Skype;
import com.skype.SkypeException;

@SpringBootApplication
@EnableMongoRepositories
@EnableScheduling
public class BuddySupportApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = SpringApplication.run(BuddySupportApplication.class, args);
		try {
			Skype.addApplication("Skype4Java");
			Skype.addChatMessageListener(ctx.getBean(ChatListner.class));
		} catch (SkypeException e) {
			e.printStackTrace();
		}
	}
}
