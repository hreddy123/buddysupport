package com.ccclogic.hackathon.buddySupport.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ccclogic.hackathon.buddySupport.dao.ConnectionRequestRepository;
import com.ccclogic.hackathon.buddySupport.dao.CustomerConnectionRepository;
import com.ccclogic.hackathon.buddySupport.dao.CustomerRepository;
import com.ccclogic.hackathon.buddySupport.models.ConnectRequest;
import com.ccclogic.hackathon.buddySupport.models.Customer;
import com.ccclogic.hackathon.buddySupport.models.RegisterResponse;
import com.ccclogic.hackathon.buddySupport.models.SupportStatus;
import com.skype.Friend;
import com.skype.Skype;
import com.skype.SkypeException;
import com.skype.User.BuddyStatus;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class SupportController {

	@RequestMapping(value = "/connect", method = RequestMethod.POST)
	public int count(ConnectRequest skyperequest, HttpSession session) {
		try {
			skyperequest.setConnectedAt(new DateTime());
			session.setAttribute("request", skyperequest.getId());
			log.debug("Skype >> checking for online buddies");
			final Friend[] friends = Skype.getContactList().getAllFriends();
			log.debug("Skype >> got online buddies");
			for( Friend frnd :Arrays.asList(friends)){
				if(frnd.getId().equalsIgnoreCase(skyperequest.getFromSkypeId())){
					if(Skype.getContactList().getFriend(skyperequest.getFromSkypeId()).getBuddyStatus()==BuddyStatus.ADDED){
						final ConnectRequest savedRequest = requests.save(skyperequest);
						final List<String> ids = new ArrayList<>();
						for(Friend send : friends){
							if(send.getId()!=frnd.getId())
							ids.add(send.getId());
						}
						final List<Customer> toMessage = customers.findByStatusAndSkypeIdIn(SupportStatus.NOT_CONTACTED,ids);

						new Thread(new Runnable() {
							
							@Override
							public void run() {
								
								log.debug("Found >> " + toMessage.size() + "Users ");
								for(Customer to : toMessage){
									try {
										log.debug("Sending to" + to.getSkypeId());
										Skype.chat(to.getSkypeId()).send("A user asked \n" + savedRequest.getQuestion() + "\n Are you in mood to help your buddy\nReply "+savedRequest.getId()+" \n to help");
									} catch (SkypeException e) {
										e.printStackTrace();
									}
									to.setBusyWith(savedRequest);
									to.setStatus(SupportStatus.AWAITING_RESPONSE);
									template.save(to);
								}
							}
						}).start();
						return toMessage.size();
						}
				}
			}
			
			
		return -1;	
		} catch (SkypeException e) {
			e.printStackTrace();
		}
		return 0;
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public RegisterResponse register(Customer customer) {
		try {
			customers.save(customer);
			Skype.getContactList().addFriend(customer.getSkypeId(),
					"I am a bot.You are getting this request because you agreed to help fellow users");
		} catch (Exception e) {
			log.error("Full stack trace", e);
			return new RegisterResponse("Skype throwed error");
		}
		return new RegisterResponse("User added to skype list");
	}

	@RequestMapping(value = "/anyone", method = RequestMethod.GET)
	public String sendHelpfulUser(HttpSession session) {
		 ConnectRequest helpingBuddy = requests.findOne((String) session.getAttribute("request"));
		if (helpingBuddy.getServedBy()==null) {
			return new String("No one willing to help");
		} else {
			return "Found one";
		}

	}
	
	@Autowired
	private MongoTemplate template;

	@Autowired
	private CustomerConnectionRepository connections;

	@Autowired
	private CustomerRepository customers;
	
	@Autowired
	private ConnectionRequestRepository requests;

}
