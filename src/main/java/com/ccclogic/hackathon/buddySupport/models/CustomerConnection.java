package com.ccclogic.hackathon.buddySupport.models;

import org.springframework.data.annotation.Id;

import lombok.Data;

@Data
public class CustomerConnection {
	public CustomerConnection(ConnectRequest helpRequest) {
		from=helpRequest;
	}
	@Id
	String id;
	ConnectRequest from;
	Customer to;

}
