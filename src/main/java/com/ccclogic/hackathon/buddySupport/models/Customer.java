package com.ccclogic.hackathon.buddySupport.models;

import org.springframework.data.annotation.Id;

import lombok.Data;

@Data
public class Customer {

    private String firstName;
    private String lastName;
    
    @Id
    private String skypeId;
    
    private SupportStatus status;
    private ConnectRequest busyWith;

    public Customer() {
    	status = SupportStatus.NOT_CONTACTED;
    }

    public Customer(String skypeid,String firstName, String lastName) {
    	this.skypeId = skypeid;
        this.firstName = firstName;
        this.lastName = lastName;
        status = SupportStatus.NOT_CONTACTED;
    }

    @Override
    public String toString() {
        return String.format(
                "Customer[id=%s, firstName='%s', lastName='%s']",
                skypeId, firstName, lastName);
    }

}