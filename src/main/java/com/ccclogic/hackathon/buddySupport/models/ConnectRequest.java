package com.ccclogic.hackathon.buddySupport.models;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

import lombok.Data;

@Data
public class ConnectRequest {
	
	@Id
	String id;
	
	String question;
	String name;
	
	DateTime connectedAt;
	
	String servedBy;
	
	String fromSkypeId;

}
