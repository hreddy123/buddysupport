package com.ccclogic.test.buddySupport;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.ccclogic.hackathon.buddySupport.BuddySupportApplication;
import com.ccclogic.hackathon.buddySupport.controllers.SupportController;
import com.ccclogic.hackathon.buddySupport.dao.CustomerRepository;
import com.ccclogic.hackathon.buddySupport.models.Customer;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BuddySupportApplication.class)
@WebAppConfiguration
@Slf4j
public class TestSkypeCount {

	private MockMvc mvc;
	
	@Autowired
	private CustomerRepository customerRepo;

	@Before
	public void setUp() throws Exception {
		mvc = MockMvcBuilders.standaloneSetup(new SupportController()).build();
	}

	@Test
	public void getCount() throws Exception {
		mvc.perform(MockMvcRequestBuilders.post("/connect").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().string("1"));
	}
	
	@Test
	public void testRegister() throws Exception {
		customerRepo.save(new Customer("himanshu.sri27","dfgdf", "sdfsdfsd"));
	}
}